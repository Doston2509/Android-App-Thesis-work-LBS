package rtuthesisproject.rtu.lv.locationbasedservices.Database;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rtuthesisproject.rtu.lv.locationbasedservices.R;

/**
 * Created by Doston on 12/7/2017.
 */

public class Coordinate_List_Adapter extends ArrayAdapter {

    List list = new ArrayList();
    public Coordinate_List_Adapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }
    static class LayoutHandler{
        TextView empty, ID, LAT, LON, DATE, TITLE, DESC, URL, PHONE, ROUTE;
    }

    public void add(Coordinates object){
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount(){
        return list.size();
    }
    @Override
    public Object getItem(int position){
        return list.get(position);
    }
    @Override
    public View getView(int position, View convertViewView, ViewGroup parent){

        View view = convertViewView;
        LayoutHandler layoutHandler;
        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.list_view_coordinates, parent, false);
            layoutHandler = new LayoutHandler();

            layoutHandler.empty = (TextView) view.findViewById(R.id.textView01);
            
            layoutHandler.ID = (TextView) view.findViewById(R.id.textView0);
            layoutHandler.LAT = (TextView) view.findViewById(R.id.textView1);
            layoutHandler.LON = (TextView) view.findViewById(R.id.textView2);
            layoutHandler.DATE = (TextView) view.findViewById(R.id.textView3);
            layoutHandler.TITLE = (TextView) view.findViewById(R.id.textView4);
            layoutHandler.DESC = (TextView) view.findViewById(R.id.textView5);
            layoutHandler.URL = (TextView) view.findViewById(R.id.textView6);
            layoutHandler.PHONE = (TextView) view.findViewById(R.id.textView7);
            layoutHandler.ROUTE = (TextView) view.findViewById(R.id.textView8);

            view.setTag(layoutHandler);

        } else {
            layoutHandler = (LayoutHandler) view.getTag();
        }

        Coordinates dataProvider = (Coordinates) this.getItem(position);
        layoutHandler.ID.setText("ID: " + dataProvider.getId()+" ");
        layoutHandler.LAT.setText("Latitute: " + dataProvider.getLat());
        layoutHandler.LON.setText("Longitute: " + dataProvider.getLon());
        long time = Long.parseLong(dataProvider.getWrite_time());
        Date date = new java.util.Date(time*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);
        layoutHandler.DATE.setText("Date: " + formattedDate);
        layoutHandler.TITLE.setText("Title: " + dataProvider.getTitle());
        layoutHandler.DESC.setText("Note: " + dataProvider.getDescription());
        layoutHandler.URL.setText("Image: " + dataProvider.getPicture_url());
        layoutHandler.PHONE.setText("Phone: " + dataProvider.getPhone());
        layoutHandler.ROUTE.setText("Route id: " + dataProvider.getRoute_id());
        layoutHandler.empty.setText("--- Coordinate ---");
        return view;
    }

}
