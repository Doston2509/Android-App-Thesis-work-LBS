package rtuthesisproject.rtu.lv.locationbasedservices;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * Source code by Duke on 9/7/2015.
 * Edited by Doston on 23.03.2018.
 *
 * A class for tracking phone coordinates
 */

public class GpsTracker extends Service implements LocationListener
{
    private final Context mContext;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    Location location;
    double latitude;
    double longitude;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // metres
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; //1 minute
    protected LocationManager locationManager;

    public GpsTracker(Context context)   {
        this.mContext = context;
        getLocation();
    }
   // @TargetApi(23)
    /**
     * Gets the phone's current location.
     */
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!isGPSEnabled && !isNetworkEnabled)   {

            }
            else{
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                if (isGPSEnabled)  {
                    if (location == null)    {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null)  {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
        }
        catch (SecurityException e)
        {
            e.printStackTrace();
        }
        return location;
    }

    public  double getLatitude()  {
        if(location != null)      {
            latitude = location.getLatitude();    }
        return latitude;
    }

    public double getLongitude()    {
        if(location != null)       {
            longitude = location.getLongitude();
        }
        return longitude;
    }

    public long getTimeStamp()    {
        return System.currentTimeMillis();
    }
    /**
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    public void onLocationChanged(Location location) {
    }
    /**
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    public void onProviderDisabled(String provider) {
    }
    /**
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    public void onProviderEnabled(String provider)   {
    }
    /**
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
    /**
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
    /**
     * Convenience method, skeletton for future use. No implementations yet.
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

/*    public void showSettingsAlert()   {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("GPS Kapalı");
        alertDialog.setMessage("Konum bilgisi alınamıyor. Ayarlara giderek gps'i aktif hale getiriniz.");
        alertDialog.setPositiveButton("Ayarlar", new DialogInterface.OnClickListener()    {
            public void onClick(DialogInterface dialog,int which)          {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });


        alertDialog.setNegativeButton("İptal", new DialogInterface.OnClickListener()       {
            public void onClick(DialogInterface dialog, int which)      {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }*/

    /**
     * Convenience method, skeletton for future use. No implementations yet.
     */
    public void stopUsingGPS()   {
        if(locationManager != null)        {
            locationManager.removeUpdates(GpsTracker.this);
        }
    }
    }
