package rtuthesisproject.rtu.lv.locationbasedservices;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import rtuthesisproject.rtu.lv.locationbasedservices.Database.MyDatabaseHelper;

/**
 * @author Doston, on 20.03.2018.
 * This is a DialogFragment that is called:
 * 1) when user has retrieved its location and wants to post it long with some notes.
 * 2) user is tracking a route and wants to single out one point and add notes about it.
 *
 */

public class DF_Send extends DialogFragment {
    private String phone = Login.inputPhone;
    private OnFragmentInteractionListener mListener;

    MyDatabaseHelper myDB;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;
    String phone_user;

    public DF_Send() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Double lat = getArguments().getDouble("lat");
        final Double lon = getArguments().getDouble("long");
        final boolean trackon = getArguments().getBoolean("trackon");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.moreinfo));
        builder.setCancelable(false);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.df_send, null);
        builder.setView(v);
        final EditText et_notes = (EditText) v.findViewById(R.id.et_notes);
        final EditText et_title = (EditText) v.findViewById(R.id.et_title);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String title = et_title.getText().toString();
                String notes = et_notes.getText().toString();;
                if (trackon == true){
                    mListener.onPointAdded(title, notes);
                }
                else {
                    myDB = new MyDatabaseHelper(getContext());
                    sqLiteDatabase = myDB.getReadableDatabase();
                    Long time1 = System.currentTimeMillis();
                    myDB.addPoint(lat.toString(), lon.toString(), time1.toString(), title, notes, phone, sqLiteDatabase);

/**
                final Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(MapsActivity.BASE_URL)
                        .build();
                I_Retrofit service = retrofit.create(I_Retrofit.class);
                Long time = System.currentTimeMillis();
                Call<ResponseBody> call = service.postPoint(lat.toString(), lon.toString(),
                        time, title, notes,"", phone, 1);
                  call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                       if (response.isSuccessful()){
                           //TODO this sometimes breaks the app, sort the Toast
                           Log.d("sending a coord", "sending a coord");
                     //  Toast.makeText(getActivity(),"Your location successfully saved", Toast.LENGTH_SHORT).show();
                       }
                      // else{
                      //     Toast.makeText(getActivity(), "Error: " + response.code(), Toast.LENGTH_SHORT).show();
                    //   }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                      // Toast.makeText(getActivity(), "Network error..." , Toast.LENGTH_SHORT).show();

                    }
                });

                  */
            }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onPointAdded(String title, String notes);
    }
}
