package rtuthesisproject.rtu.lv.locationbasedservices.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.w3c.dom.ProcessingInstruction;

/**
 * Created by Doston on 12/15/2017.
 */

public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "FINAL_DATABASE.db";
    private static final String CREATE_USERS_TABLE = "CREATE TABLE `users` (" +
            "`phone` TEXT PRIMARY KEY, " +
            "`password` TEXT, " +
            "`email` TEXT," +
            "`google_map` TEXT," +
            "`last_update` TEXT," +
            "`track` TEXT);";

    private static final String CREATE_COODINATES_TABLE = "CREATE TABLE `coordinates` ( " +
            "`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "`lat` TEXT, " +
            "`lon` TEXT, " +
            "`write_time` TEXT, " +
            "`title` TEXT, " +
            "`description` TEXT, " +
            "`picture_url` TEXT, " +
            "`phone` TEXT, " +
            "`route_id` TEXT, " +
            "FOREIGN KEY(phone) REFERENCES users(phone), " +
            "FOREIGN KEY(route_id) REFERENCES route(route_id));";

    private static final String CREATE_ROUTE_TABLE = "CREATE TABLE `route` ( " +
            "`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "`name` TEXT, " +
            "`start_time` TEXT, " +
            "`phone` TEXT, " +
            "FOREIGN KEY (phone) REFERENCES users (phone));";

    public static final String CREATE_TRAVEL_LIST_TABLE = "" +
            "CREATE TABLE `travel_list` ( " +
            "`id_` INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "`date_from` INTEGER, " +
            "`date_to` INTEGER, " +
            "`title` TEXT, " +
            "`description` TEXT, " +
            "`phone` TEXT, " +
            "FOREIGN KEY (`phone`) REFERENCES `users` (`phone`));";
    public static final String USER_ADMIN = "INSERT INTO `users` VALUES " +
            "('28786188','admin','admin@admin.com','AIzaSyAr3WXVrYpIZDpPPKStNlsyDdVdsRTZSa8',1521633162,'Travelling'), " +
            "('123456','abcde','abcde@admin.com','AIzaSyAr3WXVrYpIZDpPPKStNlsyDdVdsRTZSa8',1521632362,'Awesome Trip');";

    public static final String ONE_1 = "INSERT INTO `coordinates` (lat, lon, write_time, title, description, picture_url, phone, route_id) VALUES ('56.947204','24.1029806',1486904229,'Riga Technical University','Port of Riga','https://ic.pics.livejournal.com/samokats/66043854/508166/508166_900.jpg','28786188',1),"+
"('56.9483217','24.11092',1450702629,'Freedom Monument','Part of Riga','https://ic.pics.livejournal.com/samokats/66043854/508166/508166_900.jpg','28786188',1),"+
        "('56.9483217','24.11092',1434373029,'University of Latvia','Part of Riga','https://ic.pics.livejournal.com/samokats/66043854/508166/508166_900.jpg','28786188',1);";
    public static final String ONE_2 = "INSERT INTO `coordinates` (lat, lon, write_time, title, description, picture_url, phone, route_id) VALUES ('56.951191','24.1067585', 1471265829,'Latvijas Kara muzejs','War museum in Riga','http://idoston.com/','28786188',1);";
    public static final String ONE_3 = "INSERT INTO `coordinates` (lat, lon, write_time, title, description, picture_url, phone, route_id) VALUES ('56.9506118','24.1120586', 1506344229,'Laima clock','Symbol of Riga','http://idoston.com/','28786188',1);";
    public static final String ONE_4 = "INSERT INTO `coordinates` (lat, lon, write_time, title, description, picture_url, phone, route_id) VALUES ('56.9475827','24.1136666', 1518699429,'Origo center','Shopping center','http://idoston.com/','28786188',1);";
    public static final String ONE_5 = "INSERT INTO `coordinates` (lat, lon, write_time, title, description, picture_url, phone, route_id) VALUES ('56.9500727','24.1140226', 1443704229,'CopyPro','Print/Copy center','http://idoston.com/','28786188',1);";


    public static final String TWO_2 = "INSERT INTO `coordinates` (lat, lon, write_time, title, description, picture_url, phone, route_id) VALUES ('54.6873281','23.1641196', 1443704229,'Vilnius','Vilnius, Lithuania','http://idoston.com/','28786188',2),\n" +
            "('54.0891755','26.5656624', 1443704229,'Minsk','Minsk, Belarus','http://idoston.com/','28786188',2)," +
            "('52.4421669','13.713649', 1443704229,'Berlin','Berlin Schönefeld Airport','http://idoston.com/','28786188',2)," +
            "('51.6488473','10.58653', 1443704229,'Frankfurt','Frankfurt University of Applied Sciences','http://idoston.com/','28786188',2)," +
            "('49.0315241','2.3271879', 1443704229,'Eiffel Tower','Paris, France','http://idoston.com/','28786188',2)," +
            "('52.4421669','13.713649', 1443704229,'Berlin','Berlin Schönefeld Airport','http://idoston.com/','28786188',2)," +
            "('40.6925824','8.3605174', 1443704229,'Sassari','Sassari, Italy','http://idoston.com/','28786188',2)," +
            "('41.424453','11.993268', 1443704229,'Colosseum','Rome, Italy','http://idoston.com/','28786188',2),\n" +
            "('39.2857564','64.8415387', 1443704229,'Samarkand','Samarkand, Uzbekistan','http://idoston.com/','28786188',2)," +
            "('40.6334126','67.8800795', 1443704229,'Tashkent','Tashkent, Uzbekistan','http://idoston.com/','28786188',2);";

    public static final String THREE_3 = "INSERT INTO `route` VALUES (1,'DEFAULT','0000000000','28786188')," +
            "(2,'My trip','1521634162','28786188')," +
            "(3,'Berlin trip','1521633662','28786188')," +
            "(4,'routeName','00000000','28786188')," +
            "(5,'routeName','00000000','28786188')," +
            "(6,'again1','1522064673407','28786188')," +
            "(7,'again2','1522064811192','28786188')," +
            "(8,'again3','1522064950937','28786188')," +
            "(9,'again4','1522065200766','28786188')," +
            "(10,'prrr','1522066188688','28786188');";

    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

//        SQLiteDatabase db = this.getWritableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_USERS_TABLE);
        sqLiteDatabase.execSQL(CREATE_ROUTE_TABLE);
        sqLiteDatabase.execSQL(CREATE_COODINATES_TABLE);
        sqLiteDatabase.execSQL(CREATE_TRAVEL_LIST_TABLE);
        sqLiteDatabase.execSQL(USER_ADMIN);
        sqLiteDatabase.execSQL(ONE_1);
        sqLiteDatabase.execSQL(ONE_2);
        sqLiteDatabase.execSQL(ONE_3);
        sqLiteDatabase.execSQL(ONE_4);
        sqLiteDatabase.execSQL(ONE_5);
        sqLiteDatabase.execSQL(TWO_2);
        sqLiteDatabase.execSQL(THREE_3);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {


    }
//    public void insertData(String name, int ranking, String tuition_fee, String program, String city, String country, String continent){
//
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(COL_2, name);
//        contentValues.put(COL_3, ranking);
//        contentValues.put(COL_4, tuition_fee);
//        contentValues.put(COL_5, program);
//        contentValues.put(COL_6, city);
//        contentValues.put(COL_7, country);
//        contentValues.put(COL_8, continent);
//        db.execSQL();
//        db.insert(TABLE_NAME, null, contentValues);
//
//    }
    public boolean authernticateUser(SQLiteDatabase sqLiteDatabase, String phone, String pass){

        String one = String.format("select * from users where phone = '%s' and password = '%s';", phone, pass);

        Cursor cursor =  sqLiteDatabase.rawQuery(one, null);
        if (cursor.getCount() > 0){
            return true;
        } else {
            return false;
        }
    }
    public Cursor getAllcoordinates(SQLiteDatabase sqLiteDatabase, String phone){
        String one = String.format("select lat, lon from coordinates where phone = '%s';", phone);

        Cursor cursor =  sqLiteDatabase.rawQuery(one, null);
        return cursor;
    }

    public Cursor getInfoForOne(SQLiteDatabase sqLiteDatabase, String lat, String lon){
        String one = String.format("select * from coordinates where lat = '%s' and lon = '%s';", lat, lon);

        Cursor cursor =  sqLiteDatabase.rawQuery(one, null);
        return cursor;
    }

    public void addPoint(String lat, String lon, String date, String title, String description, String phone, SQLiteDatabase sql){


        ContentValues contentValues = new ContentValues();
        contentValues.put("lat", lat);
        contentValues.put("lon", lon);
        contentValues.put("write_time", date);
        contentValues.put("title", title);
        contentValues.put("description", description);
        contentValues.put("picture_url", "http://idoston.com/images");
        contentValues.put("phone", phone);
        contentValues.put("route_id", "2");
        sql.insert("coordinates", null, contentValues);

        Log.e("DATABSE OPERATION", "One row is inserted ...");
    }
    public void addNewUser(String phone, String password, String email, String google_map, String last_update, String track, SQLiteDatabase sql){


        ContentValues contentValues = new ContentValues();
        contentValues.put("phone", phone);
        contentValues.put("password", password);
        contentValues.put("email", email);
        contentValues.put("google_map", google_map);
        contentValues.put("last_update", last_update);
        contentValues.put("track", track);
        sql.insert("users", null, contentValues);

        Log.e("DATABSE OPERATION", "New user is inserted ...");
    }
    public void addRoute(String name, String start_time, String phone, SQLiteDatabase sqLiteDatabase){
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("start_time", start_time);
        contentValues.put("phone", phone);
        sqLiteDatabase.insert("route", null, contentValues);

        Log.e("DATABSE OPERATION", "New route is inserted ...");
    }

    public Cursor getUsersList(SQLiteDatabase db){
        Cursor cursor;

        String[] projection = {"phone", "password", "email", "google_map", "last_update", "track"};

        cursor = db.query("users", projection, null, null, null, null, null);
        return cursor;
    }
    public Cursor getCoordinatesList(SQLiteDatabase db){
        Cursor cursor;

        String[] projection = {"id", "lat", "lon", "write_time", "title", "description", "picture_url", "phone", "route_id"};

        cursor = db.query("coordinates", projection, null, null, null, null, null);
        return cursor;
    }
//
//    public Cursor getAdaptiveData(SQLiteDatabase sqLiteDatabase, int min, int max, String continent){
//
//        Cursor res = sqLiteDatabase.rawQuery(" select * from " + TABLE_NAME +
//                " as U where (U.Ranking < " + max + " AND U.Ranking >  " + min +
//                " AND U.Continent = '" + continent + "');", null);
//
//        return res;
//    }
}
