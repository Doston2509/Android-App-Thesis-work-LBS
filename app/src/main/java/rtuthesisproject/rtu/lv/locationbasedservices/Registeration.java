package rtuthesisproject.rtu.lv.locationbasedservices;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.MyDatabaseHelper;

/**
 * @author Doston
 *
 */

public class Registeration extends AppCompatActivity {

    private EditText user_phone;
    private EditText user_pass_;
    private EditText user_email_;
    private Button signing;
    private String phone, pass, email;
    private long last_update;
    private ProgressBar progressBar;
    private static final String google_map_key = "AIzaSyAr3WXVrYpIZDpPPKStNlsyDdVdsRTZSa8";;
    private static String track = "Riga trip";


    //For video
    private VideoView videoBG;
    MediaPlayer mMediaPlayer;
    int mCurrentVideoPosition;


    //Database
    MyDatabaseHelper myDB;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;

    /**
     * @author Doston
     *
     * The method is created to start activity of Registration and it has several tasks to do such as
     * there, all elements are initialized as soon as Registration activity is started and video will be
     * played as background of Registration activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);

        user_phone = (EditText) findViewById(R.id.sg_phone);
        user_pass_ = (EditText) findViewById(R.id.sg_pass);
        user_email_ = (EditText) findViewById(R.id.sg_email);
        signing = (Button) findViewById(R.id.btn_sg);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        videoBG = (VideoView) findViewById(R.id.videoView);

//        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.plane);
//        // and then finally add your video resource. Make sure it is stored
//
//        // Set the new Uri to our VideoView
//        videoBG.setVideoURI(uri);
//        // Start the VideoView
//        videoBG.start();
//
//        // Set an OnPreparedListener for our VideoView. For more information about VideoViews,
//        videoBG.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mediaPlayer) {
//                mMediaPlayer = mediaPlayer;
//                // We want our video to play over and over so we set looping to true.
//                mMediaPlayer.setLooping(true);
//                // We then seek to the current posistion if it has been set and play the video.
//                if (mCurrentVideoPosition != 0) {
//                    mMediaPlayer.seekTo(mCurrentVideoPosition);
//                    mMediaPlayer.start();
//                }
//            }
//        });
    }

    /**
     * @author Doston
     * This method sends data to the server
     * @param view comes from button view, when the button is pressed, this view is taken
     */
    public void sendUserData(View view){

        progressBar.setVisibility(View.VISIBLE);
        signing.setClickable(false);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                signing.setClickable(true);

            }
        }, 3000);


        if (getDataOfUser()){
            myDB = new MyDatabaseHelper(this);
            sqLiteDatabase = myDB.getReadableDatabase();
            Long time1 = System.currentTimeMillis();
            myDB.addNewUser(phone, pass, email, google_map_key, time1.toString(), track, sqLiteDatabase);
            startActivity(new Intent(Registeration.this, Login.class));
            //send_data_to_server(phone, pass, email, google_map_key, track);
        } else {
            Toast.makeText(Registeration.this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    /**
     *  @author Doston
     *
     * The method gets all input values from user when sendUserData method is called by pressing "Sign up"
     * button from user and it assigns values to specified fields and it checks each field is filled by user,
     * if any field is not filled it calls ask_user_data() and passes missed field name, there ask_user_data() methos
     * will display message box for user, then this method returns false. If each field is filled then this method
     * will return true. The method is used in sendUserData() to check each fiels is taken in IF statement.
     * @return - returns to boolean value true if all values are provided by user or otherwise false
     */
    public boolean getDataOfUser(){
        phone = user_phone.getText().toString().trim();
        pass = user_pass_.getText().toString().trim();
        email = user_email_.getText().toString().trim();
        if (phone.equals("")){
            ask_user_data("Phone");
            return false;
        } else if ( pass.equals("")){
            ask_user_data("Pass");
            return false;
        } else if (email.equals("")){
            ask_user_data("Email");
            return false;
        }
        return true;
    }

    /**
     *  @author Doston
     *
     * The method is used when the user misses any value such as phone, email, password
     * and the method is called from getDataofUser() method. This method is used in Registration
     * activity on "Sign up" buton.
     * @param str
     */
    public void ask_user_data(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Registration");
        builder.setMessage(str + " is empty, please fill in it and contunie");
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

    }

    /**
     *  @author Doston
     *
     * The method takes input values and sends to server, here the method is connected with I_Retrofit class
     * and there is defined service.postPoint method which takes values and sends to server,
     * here all values come from user input and sends to I_Retrofit.postPoint() method.
     *
     * If data is sent succesfully to server, onResponse is called, otherwise onFailed method will be called
     *
     * @param phone1
     * @param pass1
     * @param email1
     * @param google_map1
     * @param track1
     */
    public void send_data_to_server(String phone1, String pass1, String email1, String google_map1, String track1){

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MapsActivity.BASE_URL) //http://192.168.8.216:8080/api/coordinates/
                .build();
        I_Retrofit service = retrofit.create(I_Retrofit.class);
        Long time = System.currentTimeMillis();
        Integer timeint = time.intValue();
        Call<ResponseBody> call = service.postPoint(phone1, pass1, email1, google_map1, timeint, track1);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(Registeration.this, "New user is saved", Toast.LENGTH_LONG).show();
                    Log.d("User Data is sent ", "" + response.code() + " " + call.request().url());
                    startActivity(new Intent(Registeration.this, Login.class));
                } else {
                    Toast.makeText(Registeration.this, "Bad response from server", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //   Toast.makeText(getActivity(), "Saving new location failed... " + t.getCause(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     *  @author Doston
     *
     * the method is genereated for videoView, there is video as background in Registration activity
     * when the Registration activity is open and video is played automatically and this method is used onPause,
     * but the video (in background) is not clickable
     */
    @Override
    protected void onPause() {
        super.onPause();
        // Capture the current video position and pause the video.
        mCurrentVideoPosition = mMediaPlayer.getCurrentPosition();
        videoBG.pause();
    }

    /**
     *  @author Doston
     *
     * the method is used to restart the video when it finishes or when the Registration activity is open
     * or started again.
     */
    @Override
    protected void onResume() {
        super.onResume();
        // Restart the video when resuming the Activity
        videoBG.start();
    }

    /**
     *  @author Doston
     *
     * The method is called when the Registration activity is destroyed or closed, it will release the
     * MediaPlayer (which plays video) and set MediaPlayer to null so that MediaPlayer finishes its
     * job
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // When the Activity is destroyed, release our MediaPlayer and set it to null.
        mMediaPlayer.release();
        mMediaPlayer = null;
    }
}
