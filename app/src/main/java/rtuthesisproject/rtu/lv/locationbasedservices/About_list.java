package rtuthesisproject.rtu.lv.locationbasedservices;

public class About_list {
    private int imgURL;
    private String titleRTU;
    private String author;
    private String titleThesis;
    private String description;
    private String contact;
    private String studentID;

    public About_list(int imgURL, String titleRTU, String author, String studentID, String titleThesis, String description, String contact) {
        this.imgURL = imgURL;
        this.titleRTU = titleRTU;
        this.author = author;
        this.titleThesis = titleThesis;
        this.description = description;
        this.contact = contact;
        this.studentID = studentID;
    }

    public int getImgURL() {
        return imgURL;
    }

    public void setImgURL(int imgURL) {
        this.imgURL = imgURL;
    }

    public String getTitleRTU() {
        return titleRTU;
    }

    public void setTitleRTU(String titleRTU) {
        this.titleRTU = titleRTU;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitleThesis() {
        return titleThesis;
    }

    public void setTitleThesis(String titleThesis) {
        this.titleThesis = titleThesis;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }
}
