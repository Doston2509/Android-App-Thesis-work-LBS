package rtuthesisproject.rtu.lv.locationbasedservices.Database;

/**
 * Created by Doston on 4/28/2018.
 */

public class Coordinates {
    private int id;
    private String lat;
    private String lon;
    private String Write_time;
    private String Title;
    private String Description;
    private String Picture_url;
    private String Phone;
    private String route_id;

    public Coordinates(int id, String lat, String lon, String write_time, String title, String description, String picture_url, String phone, String route_id) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        Write_time = write_time;
        Title = title;
        Description = description;
        Picture_url = picture_url;
        Phone = phone;
        this.route_id = route_id;
    }

    public int getId() {
        return id;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getWrite_time() {
        return Write_time;
    }

    public String getTitle() {
        return Title;
    }

    public String getDescription() {
        return Description;
    }

    public String getPicture_url() {
        return Picture_url;
    }

    public String getPhone() {
        return Phone;
    }

    public String getRoute_id() {
        return route_id;
    }
}
