package rtuthesisproject.rtu.lv.locationbasedservices;

/**
 * Created by Doston on 3/27/2018.
 *
 * The class is needed to create object in CustomInfoWindowGoogleMap and also MapsActivity classes
 */

public class InfoWindowData {
    private String title;
    private String lat;
    private String lon;
    private String time;
    private String desc;

    public InfoWindowData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
