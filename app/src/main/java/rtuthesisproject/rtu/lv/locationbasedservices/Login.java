package rtuthesisproject.rtu.lv.locationbasedservices;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Geocoder;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import okhttp3.Route;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.MyDatabaseHelper;

public class Login extends AppCompatActivity {

    private CheckBox saveLoginCheckBox;
    ProgressBar progressBar;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean savedLogin;
    static String inputPhone = "28786188";
    static String inputPassword = "admin";
    boolean status = false;
    EditText user_phone_, user_pass_;


    //Database
    MyDatabaseHelper myDB;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // just check status of location service whether is enabled or not? isLocationServiceEnabled() give true/false
        Toast.makeText(Login.this, "Location enabled : " + isLocationServiceEnabled(), Toast.LENGTH_LONG).show();
        askUserToEnableLBS();
        Button loginbutton = (Button) findViewById(R.id.b_login);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        user_phone_ = (EditText) findViewById(R.id.id_text_login_phone);
        user_pass_ = (EditText) findViewById(R.id.id_text_login_pass);


        myDB = new MyDatabaseHelper(getApplicationContext());
//        PasswordText.setTypeface(Typeface.DEFAULT);
//        PasswordText.setTransformationMethod(new PasswordTransformationMethod());
        saveLoginCheckBox = (CheckBox) findViewById(R.id.checkbox_locallogin);
//        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
//        loginPrefsEditor = loginPreferences.edit();
//        savedLogin = loginPreferences.getBoolean("saveLogin", false);
//        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

////this checks if there are any saved credentials and then uses them.
//        if (savedLogin == true) {
//            UserText.setText(loginPreferences.getString("username", ""));
//            PasswordText.setText(loginPreferences.getString("password", ""));
//            saveLoginCheckBox.setChecked(true);
//            View view = getCurrentFocus();
//            if (view != null) {
//                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//            }
//        }
//
//        saveLoginCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                View view = getCurrentFocus();
//                if (view != null) {
//                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                }
//            }
//
//            ;
//        });
        loginbutton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        inputPhone = user_phone_.getText().toString();
                        inputPassword = user_pass_.getText().toString();
                        if (inputPhone.equals("")) {
                            ask_user_missed_data("Phone");
                            return;
                        }
                        if (inputPassword.equals("")) {
                            ask_user_missed_data("Password");
                            return;
                        }
                        AuthenticationUser(inputPhone, inputPassword);
                    }
                }
        );
    }



    /**
     * @author Doston
     * The method is needed to pause the video which will be played in background of Registration activity
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * @author Doston
     * The method is needed to resume the video which will be played in background of Registration activity
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * @author Doston
     * The method is used to check whether LBS is enabled or not, if user enabled the LBS it returns
     * true otherwise it returns false and usage of the method is represented in askUserToEnableLBS() method
     * @return - if LBS is enabled it returns true, otherwise false
     */
    public boolean isLocationServiceEnabled(){
        LocationManager locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            //GPS enabled
            return true;
        }else{
            //GPS disabled
            return false;
        }
    }

    /**
     * @author Doston
     * This method asks user to enable LBS (Location Based Services) if it is disabled and it
     * redirects the user. The usage of the method can be found in onCreate()
     */
    public void askUserToEnableLBS(){
        // Get Location Manager and check for GPS & Network location services
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(!isLocationServiceEnabled()) {
            // Build the alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Location Services Not Active");
            builder.setMessage("Please enable Location Services and GPS");
            builder.setPositiveButton("Turn on location", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Show location settings when the user acknowledges the alert dialog
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    arg0.cancel();
                }});
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }
    }

    /**
     * @author - Doston
     * The method is used to redirect user to Registration activity. When user press "No account? Sign Up" button
     * is clicked and Registration activity will be started
     * @param view - this parater comes from TextView element when it is pressed  by user
     */
    public void MoveRegisteration(View view){
        startActivity(new Intent(Login.this, Registeration.class));
    }

    /**
     * @author Doston
     * The method is used to authenticate user by his/her phoneNumber and password, it takes two parameters and sends to
     * server to check whether this user exists or not, if there is such user, server will return "true" otherwise "false"
     * There is return type for this method, if user exists
     * @param phone_ -  it  comes from user input Login page
     * @param pass_ - it comes from user input on Login page
     * @return - it returns true if the user  exists or false if not
     */
    public boolean AuthenticationUser(String phone_, String pass_){

//
//
//        final Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(MapsActivity.BASE_URL)
//                .build();
//        I_Retrofit service = retrofit.create(I_Retrofit.class);
//        Call<ResponseBody> userCall = service.authentication(phone_, pass_);
//        userCall.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    if (response.body() != null){
//                        if (response.body().string().equals("true")){
//                            status = true;
//                            Intent i = new Intent(getApplicationContext(), MapsActivity.class);
//                            startActivity(i);
//                        }
//                        else {
//                            LoginFailedAlertBox();
//                        }}
//                    else {
//                        Toast.makeText(Login.this, "Server error...", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(Login.this, "Network error... " + t.getCause(), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
        myDB = new MyDatabaseHelper(getApplicationContext());
        sqLiteDatabase = myDB.getReadableDatabase();

        status = myDB.authernticateUser(sqLiteDatabase, phone_, pass_);
        if (status) {
            Intent i = new Intent(getApplicationContext(), MapsActivity.class);
            i.putExtra("PHONE", inputPhone);
                            startActivity(i);

        } else {
            Toast.makeText(Login.this, "Login Failed", Toast.LENGTH_LONG).show();
        }

        return status;
    }

    /**
     * @author Doston
     * Thr method is used in login actvity, when user press "Login" button, there will be checking if
     * every things goes file, this method will not be called, if there is no connect or phone/password is
     * wrong then this method will called to warn user. The usage of the method in onCreate()`s loginButton.OnClick() action
     * can be found
     */
    public void LoginFailedAlertBox(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Login failed");
        builder.setMessage("Phone number or password is wrong");

        builder.setNegativeButton("Close",new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                arg0.cancel();
            }});
        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    /**
     * The method is used when the user misses any value such as phone, email, password
     * and the method is called from getDataofUser() method
     * @param str
     */
    public void ask_user_missed_data(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Login failed");
        builder.setMessage(str + " is empty, please fill in it and contunie");
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

}
