package rtuthesisproject.rtu.lv.locationbasedservices;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Doston on 22.03.2018.
 *
 * Retrofit interface
 */

public interface I_Retrofit {
    /**
     * @author Doston
     *
     * @return list of C_Coordinates objects
     * @param phone
     * @param route_id must be 1 to retrieve all individual points
     */
    @GET("api/coordinates/")
    Call<List<C_Coordinates>> fetchAllIndivCoors(@Query("phone") String phone,
                                                 @Query("route_id") Integer route_id);

    /**
     * @author Doston
     *
     * @param lat
     * @param lon
     * @param time
     * @param title
     * @param descr
     * @param picurl
     * @param phone
     * @return
     */
    @POST("api/coordinates/")
    //http://192.168.8.216:8080/api/coordinates/?lat=1&lon=2&write_time=12341234&description=testdescription&phone=1234
    Call<ResponseBody> postPoint(@Query("lat") String lat, @Query("lon") String lon, @Query("write_time") Long time,
                                 @Query("title") String title, @Query("description") String descr,
                                 @Query("picture_url") String picurl, @Query("phone") String phone,
                                 @Query("route_id") Integer route_id);// ', @Query("route_id") Integer routeid);

    /**
     * @author Doston
     *
     * @param phone
     * @param password
     * @param email
     * @param google_map
     * @param last_update
     * @param track
     * @return
     */
    @POST("api/user/")
     // http://192.168.8.216:8080/api/user/?phone=phone&password=password
    Call<ResponseBody> postPoint(@Query("phone") String phone, @Query("password") String password, @Query("email") String email,
                                 @Query("google_map") String google_map, @Query("last_update") Integer last_update, @Query("track") String track);

    /**  @author Doston
     * posting induvidual points to server
     * @param coordlist
     * @return -
     */
    @POST("api/coordinatesList/")
        // http://192.168.8.216:8080/api/coordinatesList/
    Call<ResponseBody> postRoutePoints(@Body List<C_Coordinates> coordlist);

    /**@author Doston
     * Posting new route when user makes new route
     * @param name - name of route
     * @param start_time - started time of route
     * @return
     */
    @POST("api/route/")
    //http://192.168.8.216:8080/api/route/?name=routeName&start_time=0000
    Call<ResponseBody> postRoute(@Query("name") String name, @Query("start_time") String start_time, @Query("phone") String phone);


    /**
     * @author Doston
     * The method is used to authenticate user by phone number and password in Login activity. If server finds
     * a user by these phone number and password, will return true, otherwise false.
     * @param phone - phone comes from user input in Login activity
     * @param password - password comes from user input in Login activity
     * @return - will return true or false according to status from server
     */
    //http://192.168.8.216:8080/api/user/?phone=__value_&password=__value__
    @GET("api/user/")
    Call<ResponseBody> authentication(@Query("phone") String phone, @Query("password") String password);


    /**
     * @author Doston
     * The method is used to get information about individual point by sending latitude and longitude to server
     * and if there is existing information about marker, and server will respond by sending all information about
     * the selected point
     * @param lat
     * @param lon
     * @return
     */
    @GET("api/coordinatesLatLon/")
    Call<C_Coordinates> getOnePoint(@Query("lat") String lat, @Query("lon") String lon);
}
