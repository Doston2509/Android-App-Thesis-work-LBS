package rtuthesisproject.rtu.lv.locationbasedservices;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import rtuthesisproject.rtu.lv.locationbasedservices.Database.Coordinates;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.MyDatabaseHelper;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.Users;

public class MapsActivity extends AppCompatActivity
        implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        DF_Send.OnFragmentInteractionListener {

    private GoogleMap mMap;
    private Location mLastKnownLocation;


    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    public static final String BASE_URL = "http://192.168.8.101:8080/";
//    public static final String BASE_URL = "http://10.2.16.66:8080/";
    private boolean mPermissionDenied = false;
    private FloatingActionButton fab;
    private FloatingActionButton fab_rec;
    private static final String TAG = "in MapsActivity: ";
    private static final String KEY_LOCATION = "KEY_LOCATION";
    private static final String KEY_CAMERA_POSITION = "KEY_CAMERA_POSITION";
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private CameraPosition mCameraPosition;
    private static Boolean pointsON = false;
    private static Boolean trackAVAILABLE = false;
    private static Boolean trackON = false;

    //For database:
    MyDatabaseHelper myDB;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;
    String phone_user;



    public static ArrayList<C_Coordinates> getCoords() {
        return coords;
    }

    public static void setCoords(ArrayList<C_Coordinates> coords) {
        MapsActivity.coords = coords;
    }

    protected static ArrayList<C_Coordinates> coords = new ArrayList<C_Coordinates>();
    private Timer timer = new Timer();
    private String phone = Login.inputPhone;

    InfoWindowData infoWindowData;

    private static int coord_id = 0;
    private static int coord_replace = 0;
    private static String title;
    private static String notes;

    /**
     * @author Doston
     * In onCreate method all main screen UI elements are enabled and the location is established
     * if a*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        enableMyLocation();
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fab = findViewById(R.id.fab);
        fab_rec = findViewById(R.id.fab_rec);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


        phone_user = getIntent().getStringExtra("PHONE");

    }
    /**
     * @author Doston
     * Inflate the menu; this adds items to the action bar if it is present.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);


        return true;
    }
    /**
     * @author Doston
     * In onOptionsItemSelected behaviour for all menu items is written.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            Intent i = new Intent(getApplicationContext(), Login.class);
            startActivity(i);
        }
        if (id == R.id.action_showall) {
            showAllPoints();
        }
        if (id == R.id.action_track) {
            if (trackAVAILABLE == false) {
                trackAVAILABLE = true;
                fab_rec.setVisibility(View.VISIBLE);
            } else if (trackAVAILABLE == true) {
                trackAVAILABLE = false;
                fab_rec.setVisibility(View.GONE);
            }

        }
        if (id == R.id.action_show_users){
            startActivity(new Intent(MapsActivity.this, View_users.class));
        } else if (id == R.id.action_show_coordinates){
            startActivity(new Intent(MapsActivity.this, ViewCordinates.class));
        } else if (id == R.id.action_show_routes){
            startActivity(new Intent(MapsActivity.this, Routes.class));
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.system_defined_cor) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("System defined route");
            builder.setMessage("Here, system will take coordinates in every 5 minitunes and it will be stored database.");
            builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(true);
            alertDialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /**
     * @author Doston
     * Manipulates the map once available. This callback is triggered when the map is ready to be used.
     * Floating Action Button behaviour is written here, therefore it is enabled only when the map is ready
     * as otherwise the buttons have no meaning.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        enableMyLocation();
        getDeviceLocation();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                LatLng point = marker.getPosition();
                String lat_ = point.latitude + "";
                String lon_ = point.longitude + "";

                get_info_about_point(lat_, lon_);
//                Toast.makeText(MapsActivity.this, "newMarker click", Toast.LENGTH_SHORT).show();

                return false;
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMap.isMyLocationEnabled() == true && mLastKnownLocation != null) {
                    //Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), 17.0f));
                    DF_Send d = new DF_Send();
                    Bundle args = new Bundle();
                    args.putBoolean("trackon", trackON);
                    coord_replace = coord_id;
                    args.putDouble("lat", mLastKnownLocation.getLatitude());
                    args.putDouble("long", mLastKnownLocation.getLongitude());
                    d.setArguments(args);
                    d.show(getSupportFragmentManager(), "sendpoint");
                } else {
                    Toast.makeText(MapsActivity.this, "Location is not enabled", Toast.LENGTH_SHORT).show();
                }
            }
        });

        fab_rec.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey)));
        fab_rec.setOnClickListener(new View.OnClickListener() {
            @TargetApi(21) //his is for calling the get drawable
            @Override
            public void onClick(View v) {
                if (trackON == false ) { //TODO check is location is enabled. or is done?
                    trackON = true;
                    fab_rec.setImageDrawable(getDrawable(R.drawable.ic_stop_white_24dp));
                    fab_rec.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    trackRoute();
                } else if (trackON == true){
                    trackON = false;
                    fab_rec.setImageDrawable(getDrawable(R.drawable.ic_fiber_manual_record_white_24dp));
                    fab_rec.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey)));
                    timer.cancel();
                    timer = new Timer();
                    DF_SendRoute d = new DF_SendRoute();
                    d.setCancelable(false);
                    d.show(getSupportFragmentManager(), "sendroute");
                }
            }

        });

    }
    /**
     * @author Doston
     *
     * Get the best and most recent location of the device, which may be null in rare cases when a location is not available.
     */
    private void getDeviceLocation() {
        // Get the best and most recent location of the device, which may be null in rare cases when a location is not available.
        try {
            Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = task.getResult();
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), 18.0f));
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.");
                        Log.e(TAG, "Exception: %s", task.getException());
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                }
            });

        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * @author Doston
     *
     * If permissions are not given, enable the location.
     */
    private void enableMyLocation() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);

        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }







    /**
     * @author Doston
     *
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    public boolean onMyLocationButtonClick() {
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }
    /**
     * @author Doston
     *
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }
    /**
     * @author Doston
     *
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }
    /**
     * @author Doston
     *
     * Convenience method, skeletton for future use. No implementations yet.
     */
    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * @author Doston
     *
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    /**
     * @author Doston
     *
     * In onSavedInstanceState method the last known location was saved.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }
    /**
     * @author Doston
     * ShowAllPoints method is called from app bar to show/hide all user's individual points on the map.
     */
    public void showAllPoints(){
        if (pointsON == false) {
            pointsON = true;


            myDB = new MyDatabaseHelper(getApplicationContext());
            sqLiteDatabase = myDB.getReadableDatabase();
            cursor = myDB.getAllcoordinates(sqLiteDatabase, phone_user);

            if(cursor.moveToFirst()) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                do {
                    double lat11 = Double.parseDouble(cursor.getString(0));
                    double lon11 = Double.parseDouble(cursor.getString(1));
                    LatLng ll = new LatLng(lat11, lon11);
                    mMap.addMarker(new MarkerOptions().position(ll));
                    builder.include(ll);
                } while (cursor.moveToNext());

                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 5));
                //padding in px, zoom level would be better
                pointsON = true;
            } else {
                Toast.makeText(MapsActivity.this, "You dont have points", Toast.LENGTH_SHORT).show();
            }
/**
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            I_Retrofit service = retrofit.create(I_Retrofit.class); //.create(RetrofitInterfaces.class); //this is how retrofit create your api
            Call<List<C_Coordinates>> call = service.fetchAllIndivCoors(phone, 100);
            call.enqueue(new Callback<List<C_Coordinates>>() {
                @Override
                public void onResponse(Call<List<C_Coordinates>> call, Response<List<C_Coordinates>> response) {
                    if (response.isSuccessful()) {
                        List<C_Coordinates> existcoords = response.body();
                        //TODO stupid check below, need to properly check if loaded.
                        if (existcoords.size() != 0 && mMap != null) {
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (C_Coordinates c : existcoords) {
                                LatLng ll = new LatLng(c.getLat(), c.getLon());
                                mMap.addMarker(new MarkerOptions().position(ll));
                                builder.include(ll);
                            }
                            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 5)); //padding in px, zoom level would be better
                            pointsON = true;
                        } else {
                            Toast.makeText(MapsActivity.this, "You have no locations yet", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MapsActivity.this, "Error code " + response.code(), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<List<C_Coordinates>> call, Throwable t) {
                    Toast.makeText(MapsActivity.this, "Network error...", Toast.LENGTH_SHORT).show(); }
            });
            */
        } else {
            mMap.clear();
            pointsON = false;
        }
    }

    /**
     * @author Doston
     *
     * trackRoute method is used to record the route and it allows to single out in every 5 seconds
     * and add notes about some points in the route.
     */
    public void trackRoute() {

        final GpsTracker gpsTracker = new GpsTracker(getApplicationContext());
        if (coords.size() != 0){  coords.clear(); }
        coord_id = 0;
        timer.scheduleAtFixedRate(new TimerTask() {
            @SuppressLint("DefaultLocale")
            @Override
            public void run() {
                Location l = gpsTracker.getLocation();
                coord_id +=1;
                coords.add(new C_Coordinates(l.getLatitude(), l.getLongitude(), gpsTracker.getTimeStamp(),
                        "", "", "", phone));
            }
        }, 0, 5000);
    }

    /**
     * @author Doston
     * Method implementation from DF_Send.OnFragmentInteractionListener.
     * Used to retrieve data from DialogFragment back to the Activity.
     *
     */
    @Override
    public void onPointAdded(String title, String notes) {
        this.title = title;
        this.notes = notes;
        C_Coordinates existing = coords.get(coord_replace);
        existing.setTitle(title);
        existing.setDescription(notes);}

    /**
     * @author Doston
     * This method is used to query information about selected marker on the map, when user presses the marker
     * and onMarkerClick() will call this method and passes two values, latitude and longitude of selected marker, then
     * this method sends that data to server to get information about the selected data. The usage of this method can
     * be found in onMarkerClick()
     * @param lat - Latitude comes from server about selected marker
     * @param lon - Longitude comes from server about selected marker
     */
    private void get_info_about_point(String lat, String lon){

        myDB = new MyDatabaseHelper(getApplicationContext());
        sqLiteDatabase = myDB.getReadableDatabase();
        cursor = myDB.getInfoForOne(sqLiteDatabase, lat, lon);

        if(cursor.moveToFirst()){
            do{
                String _lat, _lon, _title, _date, _desc;
                _lat = cursor.getString(1);
                _lon = cursor.getString(2);
                _date = cursor.getString(3);
                _title = cursor.getString(4);
                _desc = cursor.getString(5);

//                Toast.makeText(MapsActivity.this, "Title : " + _title, Toast.LENGTH_SHORT).show();
                alertForPoint(_lat, _lon, _title, _date, _desc);

            } while (cursor.moveToNext());
        } else {
            Toast.makeText(MapsActivity.this, "The point has nothing", Toast.LENGTH_SHORT).show();
        }

        /**
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        I_Retrofit service = retrofit.create(I_Retrofit.class); //.create(RetrofitInterfaces.class); //this is how retrofit create your api

        Call<C_Coordinates> call = service.getOnePoint(lat, lon);
        call.enqueue(new Callback<C_Coordinates>() {
            @Override
            public void onResponse(Call<C_Coordinates> call, Response<C_Coordinates> response) {
                if (response.isSuccessful()) {
                    C_Coordinates e = response.body();
                    Log.d("got one phone: ", " " + e.getPhone());
                    Log.d("got one descr : ", " " + e.getDescription());
                    Log.d("got one title: ", " " + e.getTitle());
                    alertForPoint(e.getLat().toString(), e.getLon().toString(), e.getTitle(), e.getWrite_time().toString(), e.getDescription());
                }
                else {
                    Log.d("got one: ", " something");
                }
            }

            @Override
            public void onFailure(Call<C_Coordinates> call, Throwable t) {
                Log.d("got one: ", " onFailure");
                ; }
        });

         */
    }

    /**
     *@author Doston
     * This method is used to pass values to Message layout and also convert Unix timestamp into
     * human readable form. The usage can be found in get_info_about_point(). When user presses the marker on the
     * map, get_info_about_point() will search information about selected marker and passess that information
     * through this method to Message layout.
     * @param lat_ - Latitude comes from server about selected marker
     * @param lon_ - Longitude comes from server about selected marker
     * @param title_ - Title comes from server about selected marker
     * @param create_time_ - Date comes from server about selected marker
     * @param desc_ - Description comes from server about selected marker
     */
    public void alertForPoint(String lat_, String lon_, String title_, String create_time_, String desc_){
        Double lat1 = Double.parseDouble(lat_);
        Double lon1 = Double.parseDouble(lon_);
        LatLng snowqualmie = new LatLng(lat1, lon1);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(snowqualmie).title(title_);
        infoWindowData = new InfoWindowData();

        long time = Long.parseLong(create_time_);
        Date date = new java.util.Date(time*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);

        infoWindowData.setTitle(title_);
        infoWindowData.setLat("Lat: " + lat_);
        infoWindowData.setLon("Lon: " + lon_);
        infoWindowData.setTime("Date: " + formattedDate);
        infoWindowData.setDesc("Note: " + desc_);


        CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(this);
        mMap.setInfoWindowAdapter(customInfoWindow);

        customInfoWindow.setTitle(title_);
        customInfoWindow.setLat("Lat: " + lat_);
        customInfoWindow.setLon("Lon: " + lon_);
        customInfoWindow.setTime("Date: " + formattedDate);
        customInfoWindow.setDesc("Note: " + desc_);

        Marker m = mMap.addMarker(markerOptions);
        m.setTag(infoWindowData);
        m.showInfoWindow();

        mMap.moveCamera(CameraUpdateFactory.newLatLng(snowqualmie));
    }

}
