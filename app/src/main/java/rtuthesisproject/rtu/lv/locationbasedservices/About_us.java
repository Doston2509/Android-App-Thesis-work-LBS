package rtuthesisproject.rtu.lv.locationbasedservices;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class About_us extends AppCompatActivity {
    final String desc_ = "This application is developed along side with\n" +
            "    Bachelor`s Thesis. Here Student has implemented 3 major features" +
            " of Location Based Services. \n\nThis application includes development of Travel Diary" +
            " app which is independently development by student to make and test" +
            " all features of LBS and use power of Google Maps. In Travel Diary, a user " +
            "can register with user`s credentials and login into app, then the user can " +
            "store the location where he or she wants in order to remember or make route from one " +
            "location to another. All location points (user saved) are stored in local database, " +
            "later user can see all points on top of the map";
    final String title_ = "Analysis of features and privacy of Location " +
            "Based Service with implementation in Mobile Application";
    final String titleRTU = "Faculty of Computer Science and Information Technology";
    final String author = "Doston Hamrakulov";
    final String contact = "If you want to see Bachelor`s Thesis, please contact to the " +
            "Faculty of Computer Science and Information Technology of Riga Technical University\n\n" +
            "Address: Meža iela 1/4, \nZemgales priekšpilsēta, \nRīga, LV-1048, Latvia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        ListView listView = (ListView) findViewById(R.id.id_list_view_about);



        About_list list = new About_list(R.drawable.rtu_logo_mini,
                titleRTU,
                author, "(Student ID: 151ADB089)",
                title_, desc_,
                contact);

        ArrayList<About_list> arrayList = new ArrayList<>();

        arrayList.add(list);

        About_us_adapter adapter = new About_us_adapter(this,
                R.layout.list_view_about, arrayList);
        listView.setAdapter(adapter);
    }
}

class About_us_adapter extends ArrayAdapter<About_list>{

    private static final String TAG = "About_us_adapter";

    private Context mContext;
    private int mResource;
    private int lastPosition = -1;

    public About_us_adapter(Context context, int resource, ArrayList<About_list> list){
        super(context, resource, list);

        this.mContext = context;
        this.mResource = resource;

    }

    private static class ViewHolder {
        ImageView imgURL;
        TextView titleRTU;
        TextView author;
        TextView titleThesis;
        TextView studentId;
        TextView description;
        TextView contact;



    }

    public View getView(int position, View convertView, ViewGroup parent){
         int _imgURL = getItem(position).getImgURL();
         String titleRTU = getItem(position).getTitleRTU();
         String author = getItem(position).getAuthor();
         String studentId = getItem(position).getStudentID();
         String titleThesis = getItem(position).getTitleThesis();
         String description = getItem(position).getDescription();
         String contact = getItem(position).getContact();

         About_list about_list = new About_list(_imgURL, titleRTU, author,
                 studentId, titleThesis, description, contact);

         final View result;

         ViewHolder holder;

         if (convertView == null){
             LayoutInflater inflater = LayoutInflater.from(mContext);
             convertView = inflater.inflate(mResource, parent, false);
             holder= new ViewHolder();
             holder.imgURL = (ImageView) convertView.findViewById(R.id.about_image);
             holder.titleRTU = (TextView) convertView.findViewById(R.id.about_rtu);
             holder.author = (TextView) convertView.findViewById(R.id.about_author);
             holder.studentId = (TextView) convertView.findViewById(R.id.about_studentID);
             holder.titleThesis = (TextView) convertView.findViewById(R.id.about_title_thesis);
             holder.description = (TextView) convertView.findViewById(R.id.about_desc);
             holder.contact = (TextView)convertView.findViewById(R.id.about_contact);

             result = convertView;

             convertView.setTag(holder);
         } else {
             holder = (ViewHolder) convertView.getTag();
             result = convertView;

         }

        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
        result.startAnimation(animation);
        lastPosition = position;

        holder.imgURL.setImageResource(about_list.getImgURL());
        holder.titleRTU.setText(about_list.getTitleRTU());
        holder.author.setText(about_list.getAuthor());
        holder.studentId.setText(about_list.getStudentID());
        holder.titleThesis.setText(about_list.getTitleThesis());
        holder.contact.setText(about_list.getContact());
        holder.description.setText(about_list.getDescription());


        return convertView;
    }


}
