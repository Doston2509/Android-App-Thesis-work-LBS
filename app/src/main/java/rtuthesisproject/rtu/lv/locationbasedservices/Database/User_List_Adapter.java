package rtuthesisproject.rtu.lv.locationbasedservices.Database;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rtuthesisproject.rtu.lv.locationbasedservices.R;

/**
 * Created by Doston on 12/7/2017.
 */

public class User_List_Adapter extends ArrayAdapter {

    List list = new ArrayList();
    public User_List_Adapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }
    static class LayoutHandler{
        TextView empty, ID, PHONE, EMAIL, PASSWORD, google_map, date, track;
    }

    @Override
    public void add(Object object){
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount(){
        return list.size();
    }
    @Override
    public Object getItem(int position){
        return list.get(position);
    }
    @Override
    public View getView(int position, View convertViewView, ViewGroup parent){

        View row = convertViewView;
        LayoutHandler layoutHandler;
        if (row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.list_view_users, parent, false);
            layoutHandler = new LayoutHandler();
            layoutHandler.ID = (TextView) row.findViewById(R.id.textView1_);
            layoutHandler.PHONE = (TextView) row.findViewById(R.id.textView3_);
            layoutHandler.EMAIL = (TextView) row.findViewById(R.id.textView2_);
            layoutHandler.PASSWORD = (TextView) row.findViewById(R.id.textView4_);
            layoutHandler.google_map = (TextView) row.findViewById(R.id.textView5_);
            layoutHandler.date = (TextView) row.findViewById(R.id.textView6_);
            layoutHandler.track = (TextView) row.findViewById(R.id.textView7_);
            layoutHandler.empty = (TextView) row.findViewById(R.id.textView0_);

            row.setTag(layoutHandler);

        } else {
            layoutHandler = (LayoutHandler) row.getTag();
        }

        Users dataProvider = (Users) this.getItem(position);
        layoutHandler.ID.setText("ID : " + dataProvider.getId());
        layoutHandler.EMAIL.setText("Email : " + dataProvider.getEmail());
        layoutHandler.PHONE.setText("Phone : " + dataProvider.getPhone());
        layoutHandler.PASSWORD.setText("Password : " + dataProvider.getPassword());
        layoutHandler.google_map.setText("Map key : " + dataProvider.getGoogle_map());
        long time = Long.parseLong(dataProvider.getDate());
        Date date = new java.util.Date(time*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);
        layoutHandler.date.setText("Date : " + formattedDate);
        layoutHandler.track.setText("Track : " + dataProvider.getTrack());
        layoutHandler.empty.setText("--- User ---");

        return row;
    }

}
