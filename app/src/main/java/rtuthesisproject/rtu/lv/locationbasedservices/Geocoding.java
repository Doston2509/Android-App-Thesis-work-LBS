package rtuthesisproject.rtu.lv.locationbasedservices;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Geocoding extends AppCompatActivity{

    EditText latitudeEdit, longitudeEdit, addressEdit;
    ProgressBar progressBar;
    TextView infoText;
    CheckBox checkBox;
    Geocoder geocoder;

    public static final int USE_ADDRESS_NAME = 1;
    public static final int USE_ADDRESS_LOCATION = 2;

    int fetchType = USE_ADDRESS_LOCATION;
    private ListView listResult;

    private static final String TAG = "MAIN_ACTIVITY_ASYNC";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geocoding);

        longitudeEdit = (EditText) findViewById(R.id.longitudeEdit);
        latitudeEdit = (EditText) findViewById(R.id.latitudeEdit);
        addressEdit = (EditText) findViewById(R.id.addressEdit);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        infoText = (TextView) findViewById(R.id.infoText);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        geocoder = new Geocoder(this);
        listResult = (ListView)findViewById(R.id.listResult);
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radioAddress:
                if (checked) {
                    fetchType = USE_ADDRESS_NAME;
                    longitudeEdit.setEnabled(false);
                    latitudeEdit.setEnabled(false);
                    addressEdit.setEnabled(true);
                    addressEdit.requestFocus();
                }
                break;
            case R.id.radioLocation:
                if (checked) {
                    fetchType = USE_ADDRESS_LOCATION;
                    latitudeEdit.setEnabled(true);
                    latitudeEdit.requestFocus();
                    longitudeEdit.setEnabled(true);
                    addressEdit.setEnabled(false);
                }
                break;
        }
    }

    public void getLocationBTN(View view) {

        List<Address> geoResult = null;

        if(fetchType == USE_ADDRESS_NAME) {
            String name = addressEdit.getText().toString();
            try {
                geoResult = geocoder.getFromLocationName(name, 5);
            } catch (IOException e) {
                Toast.makeText(Geocoding.this,
                        "Address is invalid!!",
                        Toast.LENGTH_LONG).show();
            }

        } else if(fetchType == USE_ADDRESS_LOCATION) {
            String strLat = latitudeEdit.getText().toString();
            String strLon = longitudeEdit.getText().toString();
            Double lat = Double.parseDouble(strLat);
            Double lon = Double.parseDouble(strLon);
            try {
                geoResult = findGeocoder(lat, lon);
            } catch (Exception e) {
                Toast.makeText(Geocoding.this,
                        "Coordinates are invalid!!",
                        Toast.LENGTH_LONG).show();
            }

        }
            if (geoResult != null) {
                List<String> geoStringResult = new ArrayList<String>();
                for (int i = 0; i < geoResult.size(); i++) {
                    Address thisAddress = geoResult.get(i);
                    String stringThisAddress = "";
                    for (int a = 0; a < thisAddress.getMaxAddressLineIndex(); a++) {
                        stringThisAddress += thisAddress.getAddressLine(a) + "\n";
                    }

                    stringThisAddress +=
                            "Address: " + thisAddress.getAddressLine(i) + "\n" +
                                    "CountryName: " + thisAddress.getCountryName() + "\n"
                                    + "CountryCode: " + thisAddress.getCountryCode() + "\n"
                                    + "AdminArea: " + thisAddress.getAdminArea() + "\n"
                                    + "FeatureName: " + thisAddress.getFeatureName() + "\n\n"
                                    + "Latitude: " + thisAddress.getLatitude() + "\n"
                                    + "Longitude: " + thisAddress.getLongitude();
                    geoStringResult.add(stringThisAddress);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Geocoding.this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, geoStringResult);

                listResult.setAdapter(adapter);
            }



    }

    private List<Address> findGeocoder(Double lat, Double lon){
        final int maxResults = 5;
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lon, maxResults);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return addresses;
    }
}
