package rtuthesisproject.rtu.lv.locationbasedservices;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import rtuthesisproject.rtu.lv.locationbasedservices.Database.Coordinate_List_Adapter;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.Coordinates;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.MyDatabaseHelper;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.User_List_Adapter;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.Users;

public class ViewCordinates extends AppCompatActivity {

    MyDatabaseHelper myDB;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;
    ListView listView;
    Coordinate_List_Adapter coordinates_list_adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cordinates);

        listView = (ListView) findViewById(R.id.id_list_view_coordinates);

        myDB = new MyDatabaseHelper(getApplicationContext());
        sqLiteDatabase = myDB.getReadableDatabase();
        cursor = myDB.getCoordinatesList(sqLiteDatabase);

        coordinates_list_adapter = new Coordinate_List_Adapter(getApplicationContext(), R.layout.list_view_coordinates);
        listView.setAdapter(coordinates_list_adapter);

        if(cursor.moveToFirst()){
            do{
                String id, lat, lon, date, title, desc, url, phone, route_id;
                id = cursor.getString(0);
                lat = cursor.getString(1);
                lon = cursor.getString(2);
                date = cursor.getString(3);
                title = cursor.getString(4);
                desc = cursor.getString(5);
                url = cursor.getString(6);
                phone = cursor.getString(7);
                route_id = cursor.getString(8);
                Coordinates dataProvider = new Coordinates(Integer.parseInt(id), lat, lon, date, title, desc, url, phone, route_id);
                coordinates_list_adapter.add(dataProvider);
            } while (cursor.moveToNext());
        }
    }
}
