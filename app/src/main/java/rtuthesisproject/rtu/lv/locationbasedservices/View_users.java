package rtuthesisproject.rtu.lv.locationbasedservices;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.LinkedList;

import rtuthesisproject.rtu.lv.locationbasedservices.Database.MyDatabaseHelper;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.User_List_Adapter;
import rtuthesisproject.rtu.lv.locationbasedservices.Database.Users;

public class View_users extends AppCompatActivity {

    MyDatabaseHelper myDB;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;
    ListView listView;
    User_List_Adapter user_list_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_users);

        listView = (ListView) findViewById(R.id.id_list_view_users);

        myDB = new MyDatabaseHelper(getApplicationContext());
        sqLiteDatabase = myDB.getReadableDatabase();
        cursor = myDB.getUsersList(sqLiteDatabase);

        user_list_adapter = new User_List_Adapter(getApplicationContext(), R.layout.list_view_users);
        listView.setAdapter(user_list_adapter);
        int id_ = 1;

        if(cursor.moveToFirst()){
            do{
                String phone, email, password, map, date, track;
                phone = cursor.getString(0);
                password = cursor.getString(1);
                email = cursor.getString(2);
                map = cursor.getString(3);
                date = cursor.getString(4);
                track = cursor.getString(5);
                Users dataProvider = new Users(id_, email, phone, password, map, date, track);
                user_list_adapter.add(dataProvider);
                id_ += 1;
            } while (cursor.moveToNext());
        }
    }
}
