package rtuthesisproject.rtu.lv.locationbasedservices;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Doston on 3/27/2018.
 *
 * This class is used in MapsActivity to display Message box for selected marker on the map,
 * the method takes data from MapActivity when you selects marker and assigns theat data to corresponding
 * elements of MessageBox activity which is here R.layout.map_custom_infowindow.
 */


public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    private String title;
    private String lat;
    private String lon;
    private String time;
    private String desc;

    private Context context;
    InfoWindowData infoWindowData;

    /**
     * The constructor is used to get Context of the activity which will call this class, here
     * MapsActvity calls this class and once the object is created, MapsActvity`s context will pass
     * via this constructor
     * @param ctx - takes context of the calling activity
     */
    public CustomInfoWindowGoogleMap(Context ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.map_custom_infowindow, null);

        TextView title = view.findViewById(R.id.id_title_);
        TextView lat = view.findViewById(R.id.id_lat_);
        TextView lon = view.findViewById(R.id.id_lon_);
        TextView time = view.findViewById(R.id.id_time_);
        TextView desc = view.findViewById(R.id.id_desc_);


        title.setText(this.getTitle());
        infoWindowData = (InfoWindowData) marker.getTag();

        if ( infoWindowData != null){
            lat.setText(infoWindowData.getLat());
            lon.setText(infoWindowData.getLon());
            time.setText(infoWindowData.getTime());
            desc.setText(infoWindowData.getDesc());
        } else {
            lat.setText(this.getLat());
            lon.setText(this.getLon());
            time.setText(this.getTime());
            desc.setText(this.getDesc());
        }
        return view;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


}