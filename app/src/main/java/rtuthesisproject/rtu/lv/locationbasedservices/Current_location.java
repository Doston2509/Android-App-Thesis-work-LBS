package rtuthesisproject.rtu.lv.locationbasedservices;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Current_location extends AppCompatActivity {


    private Button btn_get;

    private TextView txt_1, txt_2, txt_3, txt_4;

    private LocationManager locationManager;
    private Location location;
    private final int REQUEST_LOCATION = 1;
    private Geocoder geocoder;

    String lattitude, longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_location);

        txt_1 = (TextView) findViewById(R.id.txt_current_1);
        txt_2 = (TextView) findViewById(R.id.txt_current_2);
        txt_3 = (TextView) findViewById(R.id.txt_current_3);
        txt_4 = (TextView) findViewById(R.id.txt_current_4);
        btn_get = (Button)findViewById(R.id.get_location);

        geocoder = new Geocoder(this);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    101);
        }


        btn_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                    buildAlertMessageNoGps();
                } else {
                    getLocation();
                }
            }
        });


    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(Current_location.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (Current_location.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(Current_location.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            txt_1.setText("");
            txt_2.setText("");
            txt_3.setText("");
            txt_4.setText("");

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

                txt_2.setText("Coordinates:"+ "\n" + "Latitude: " + lattitude
                        + "\n" + "Longitude: " + longitude);
                String geo = getGeocode(latti, longi);
                txt_4.setText(geo);

            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

                txt_2.setText("Coordinates:"+ "\n" + "Latitude: " + lattitude
                        + "\n" + "Longitude: " + longitude);
                String geo = getGeocode(latti, longi);
                txt_4.setText(geo);


            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

                txt_2.setText("Coordinates:"+ "\n" + "Latitude: " + lattitude
                        + "\n" + "Longitude: " + longitude);
                String geo = getGeocode(latti, longi);
                txt_4.setText(geo);

            }else{

                Toast.makeText(this,"Unble to Trace your location",Toast.LENGTH_SHORT).show();

            }
        }
    }

    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private String getGeocode(double lat, double lon){
        final int maxResults = 1;
        List<Address> geoResult = null;
        String res = null;
        try {
            geoResult = geocoder.getFromLocation(lat, lon, maxResults);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        if(geoResult != null){
            Address thisAddress = geoResult.get(0);
            res = thisAddress.getAddressLine(0) + "\nCountry code: "
                    + thisAddress.getCountryCode() + "\nCountry: "
                    + thisAddress.getCountryName();


        }
        return res;
    }




}
