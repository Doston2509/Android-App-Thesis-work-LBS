package rtuthesisproject.rtu.lv.locationbasedservices.Database;

/**
 * Created by Doston on 4/28/2018.
 */

public class Users {
    private int id;
    private String phone;
    private String password;
    private String email;
    private String google_map;
    private String date;
    private String track;

    public Users(int id, String email, String phone, String password,  String google_map, String date, String track) {
        this.id = id;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.google_map = google_map;
        this.date = date;
        this.track = track;
    }

    public int getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getGoogle_map() {
        return google_map;
    }

    public String getDate() {
        return date;
    }

    public String getTrack() {
        return track;
    }
}
