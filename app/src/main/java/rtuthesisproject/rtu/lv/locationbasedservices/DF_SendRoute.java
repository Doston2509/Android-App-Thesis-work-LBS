package rtuthesisproject.rtu.lv.locationbasedservices;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import rtuthesisproject.rtu.lv.locationbasedservices.Database.MyDatabaseHelper;

/**
 * @author Doston, on 20.03.2018.
 * This is a DialogFragment that is called when user has recorded its route and want to post it
 * along with some notes.
 */

public class DF_SendRoute extends DialogFragment {
    private Integer routeID;
    private String phone = Login.inputPhone;
    public DF_SendRoute() { }

    MyDatabaseHelper myDB;
    Cursor cursor;
    SQLiteDatabase sqLiteDatabase;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ArrayList<C_Coordinates> cordlist = MapsActivity.coords;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.moreinfo));
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.df_sendroute, null);
        builder.setView(v);
        final EditText et_title = (EditText) v.findViewById(R.id.et_title);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String title = et_title.getText().toString();

                myDB = new MyDatabaseHelper(getContext());
                sqLiteDatabase = myDB.getReadableDatabase();
                Long time1 = System.currentTimeMillis();
                myDB.addRoute(title, time1.toString(), phone, sqLiteDatabase);

/**
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(MapsActivity.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                final I_Retrofit service = retrofit.create(I_Retrofit.class);
                Long time = System.currentTimeMillis();
                Call<ResponseBody> call = service.postRoute(title, time.toString(), phone);
                int i = 0;
                for (C_Coordinates c : cordlist) {
                    i+= 1;
                                    }
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                         try {
                           String s = response.body().string();
                           routeID = Integer.valueOf(s);
                         } catch (IOException e) { //NumberFormatException ex would be better
                            e.printStackTrace();
                        }
                       if (routeID != null) {
                           for (C_Coordinates c : cordlist) {
                                c.setRoute_id(routeID);
                            }
                            Call<ResponseBody> call2 = service.postRoutePoints(cordlist);
                        call2.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response){
                               Log.d("Post new route call2:  ", "" + response.code() + " " + call.request().url());
                            }
                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                            }
                        });
                    }
                   }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        //TODO inform user sthing went wrong
                    }
                });

                */
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                }});

        AlertDialog dialog = builder.create();
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
