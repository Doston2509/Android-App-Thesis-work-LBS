package rtuthesisproject.rtu.lv.locationbasedservices;

import com.google.gson.annotations.SerializedName;

/**
 * @author Doston, on 22.03.2018.
 * This is a class for location points, both - individual and route points.
 *
 * Field route_id is the key to establish whether the points is individual or belongs to a route.
 * If route_id = 1, it is always an individual point.
 */

public class C_Coordinates {
    private Integer id;
    @SerializedName("lat")
    private Double lat;
    @SerializedName("lon")
    private Double lon;
    @SerializedName("write_time")
    private Long write_time;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("picture_url")
    private String url;
    @SerializedName("phone")
    private String phone;
    @SerializedName("route_id")
    private Integer route_id;

    public C_Coordinates(Double lat, Double lon, Long write_time,
                         String title, String description, String url,
                         String phone) {
        this.lat = lat;
        this.lon = lon;
        this.write_time = write_time;
        this.title = title;
        this.description = description;
        this.url = url;
        this.phone = phone;
    }

    public C_Coordinates() {
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Long getWrite_time() {
        return write_time;
    }

    public void setWrite_time(Long write_time) {
        this.write_time = write_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getRoute_id() {
        return route_id;
    }

    public void setRoute_id(Integer route_id) {
        this.route_id = route_id;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }





}