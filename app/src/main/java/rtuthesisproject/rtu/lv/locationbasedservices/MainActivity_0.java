package rtuthesisproject.rtu.lv.locationbasedservices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity_0 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_0);
    }

    public void MoveGeocoding(View view){
        startActivity(new Intent(MainActivity_0.this, Geocoding.class));
    }
    public void Move_location_track(View view){
        startActivity(new Intent(MainActivity_0.this, MapsActivity.class));
    }
    public void Move_Travel_diary(View view){
        startActivity(new Intent(MainActivity_0.this, Login.class));
    }
    public void Move_current_location(View view){
        startActivity(new Intent(MainActivity_0.this, Current_location.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.second_menu, menu);

        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.id_about_us) {

            startActivity(new Intent(MainActivity_0.this, About_us.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
